# frozen_string_literal: true

module Authentication
  extend ActiveSupport::Concern

  included do
    before_action :current_user
    helper_method :current_user
    helper_method :user_signed_in?
  end

  # Check if user is authenticated
  #
  # @return [void]
  def authenticate_user!
    redirect_to sign_in_path, alert: "You need to login to access that page." unless user_signed_in?
  end

  # Log in user
  #
  # @param [User] user
  # @return [void]
  def login(user)
    reset_session
    session[:current_user_id] = user.id.to_s
  end

  # Logout
  #
  # @return [void]
  def logout
    reset_session
  end

  # Redirect if logged in
  #
  # @return [void]
  def redirect_if_authenticated
    redirect_to projects_path if user_signed_in?
  end

  # Forget user
  #
  # @param [User] user
  # @return [void]
  def forget(user)
    cookies.delete :remember_token
    user.regenerate_remember_token
  end

  # Remember user
  #
  # @param [User] user
  # @return [void]
  def remember(user)
    user.regenerate_remember_token
    cookies.permanent.encrypted[:remember_token] = user.remember_token
  end

  private

  # Fetch current user
  #
  # @return [User]
  def current_user
    Current.user ||= if session[:current_user_id]
                       find_user(id: session[:current_user_id])
                     elsif cookies.permanent.encrypted[:remember_token].present?
                       find_user(remember_token: cookies.permanent.encrypted[:remember_token])
                     end
  end

  # Check if user is signed in
  #
  # @return [Boolean]
  def user_signed_in?
    return true if AppConfig.anonymous_access

    Current.user.present?
  end

  # Find user
  #
  # @param [Hash] **args
  # @return [User]
  def find_user(**args)
    User.find_by(**args)
  rescue Mongoid::Errors::DocumentNotFound
    nil
  end
end
