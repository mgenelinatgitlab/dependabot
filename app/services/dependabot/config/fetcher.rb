# frozen_string_literal: true

module Dependabot
  module Config
    class MissingConfigurationError < StandardError; end

    class Fetcher < ApplicationService
      using Rainbow

      # @param [String] project_name
      # @param [String] branch
      # @param [Boolean] update_cache
      def initialize(project_name, branch: DependabotConfig.config_branch, update_cache: false)
        @project_name = project_name
        @branch = branch
        @update_cache = update_cache
      end

      # Dependabot config
      #
      # @return [Configuration]
      def call
        ::Configuration.new(**Options::All.new(raw_config, project_name).transform)
      end

      private

      attr_reader :project_name, :update_cache, :cache_key

      delegate :config_filename, to: DependabotConfig

      # Branch to fetch configuration from
      #
      # @return [String]
      def branch
        @branch ||= gitlab.project(project_name).default_branch
      end

      # Get dependabot.yml file contents
      #
      # @return [String]
      def raw_config
        cache_key = "#{project_name}-#{branch}-configuration"
        Rails.cache.fetch(cache_key, expires_in: 12.hours, force: update_cache) do
          log(:info, "Fetching configuration for #{project_name.bright} from #{branch.bright}")
          gitlab.file_contents(project_name, config_filename, branch)
        end
      rescue Gitlab::Error::NotFound
        raise(MissingConfigurationError, "#{config_filename} not present in #{project_name}'s branch #{branch}")
      end
    end
  end
end
