# frozen_string_literal: true

module Job
  module Routers
    module MergeRequest
      class Recreate < Base
        using Rainbow

        def initialize(project_name:, mr_iid:, discussion_id: nil)
          super(project_name: project_name, mr_iid: mr_iid)

          @discussion_id = discussion_id
        end

        def call
          super

          log(:info, "Recreating merge request #{web_url.bright}".strip)
          reply_status(<<~MSG.strip)
            :warning: `dependabot-gitlab` is recreating merge request. All changes will be overwritten! :warning:
          MSG

          recreate_mr
        rescue StandardError => e
          reply_status(":x: `dependabot-gitlab` failed to recreate merge request. :x:\n\n```\n#{e}\n```")
          raise(e)
        end

        private

        attr_reader :discussion_id

        # Recreate merge request
        #
        # @return [void]
        def recreate_mr
          container_runner_class.call(
            package_ecosystem: package_ecosystem,
            task_name: "recreate_mr",
            task_args: [project_name, mr_iid, discussion_id]
          )
        end

        # Add reply when recreate performed via comment command
        #
        # @param [String] message
        # @return [void]
        def reply_status(message)
          return unless discussion_id

          Gitlab::MergeRequest::DiscussionReplier.call(
            project_name: project_name,
            mr_iid: mr_iid,
            discussion_id: discussion_id,
            note: message
          )
        end
      end
    end
  end
end
