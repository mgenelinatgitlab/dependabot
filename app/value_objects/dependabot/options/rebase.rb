# frozen_string_literal: true

module Dependabot
  module Options
    # Rebase related options
    #
    class Rebase < OptionsBase
      include OptionsHelper

      # Transform rebase options
      #
      # @return [Hash]
      def transform
        strategy = opts[:"rebase-strategy"] || "auto"
        return { rebase_strategy: { strategy: strategy } } if strategy.is_a?(String)

        validate_config_options(RebaseStrategyConfigContract, { "rebase-strategy": strategy })

        {
          rebase_strategy: {
            strategy: strategy[:strategy] || "auto",
            on_approval: strategy[:"on-approval"],
            with_assignee: strategy[:"with-assignee"]
          }
        }
      end
    end
  end
end
