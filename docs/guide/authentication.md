# Authentication

Application supports authentication for both UI and API. By default application is set to anonymous access that allows access without authentication. To disable anonymous access, environment variable `SETTINGS__ANONYMOUS_ACCESS` should be set to `false`.

## Users

If anonymous access is disabled, user must be created in order to access UI. Currently user can be created via:

- [create_user rake](../administration/rake.md#create-user) task
- [add user](../api/reference.md#add-user) API endpoint

Once user is created, it can be used to authenticate and access both UI and API.
