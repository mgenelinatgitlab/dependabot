# dependabot-gitlab

:warning: **This project is not affiliated with, funded by, or maintained by the Dependabot team, GitHub or GitLab** :warning:
_GitLab.com makes use of [Renovate Bot](https://docs.renovatebot.com/) to help keep GitLab's software releases secure via automated Merge Requests._

**This software is Work in Progress: features will appear and disappear, API will be changed, bugs will be introduced, your feedback is always welcome!**

`dependabot-gitlab` is an application that orchestrates [dependabot-core](https://github.com/dependabot/dependabot-core) library to create dependency update merge requests for GitLab projects. This repository does not contain any code related to dependency update logic, it only provides a way to run dependabot-core in a GitLab environment.

## Documentation

Application documentation can be found at <https://dependabot-gitlab.gitlab.io/dependabot>

## Release versioning

Application versioning follows [Semantic Versioning](https://semver.org/) specification but with `alpha` suffix to indicate that application as whole is not considered stable yet.

* `MAJOR` version is bumped when breaking changes are introduced.
* `MINOR` version is bumped when new features are added.
* `PATCH` version is bumped when bugs are fixed.

## Docker images

### Core application image

* [![pulls](https://img.shields.io/docker/pulls/andrcuns/dependabot-gitlab)](https://hub.docker.com/r/andrcuns/dependabot-gitlab) [![size](https://img.shields.io/docker/image-size/andrcuns/dependabot-gitlab?label=dependabot-gitlab)](https://hub.docker.com/r/andrcuns/dependabot-gitlab)

### Ecosystem updater images

* [![pulls](https://img.shields.io/docker/pulls/andrcuns/dependabot-gitlab-npm)](https://hub.docker.com/r/andrcuns/dependabot-gitlab-npm) [![size](https://img.shields.io/docker/image-size/andrcuns/dependabot-gitlab-npm?label=dependabot-gitlab-npm)](https://hub.docker.com/r/andrcuns/dependabot-gitlab-npm)
* [![pulls](https://img.shields.io/docker/pulls/andrcuns/dependabot-gitlab-pip)](https://hub.docker.com/r/andrcuns/dependabot-gitlab-pip) [![size](https://img.shields.io/docker/image-size/andrcuns/dependabot-gitlab-pip?label=dependabot-gitlab-pip)](https://hub.docker.com/r/andrcuns/dependabot-gitlab-pip)
* [![pulls](https://img.shields.io/docker/pulls/andrcuns/dependabot-gitlab-maven)](https://hub.docker.com/r/andrcuns/dependabot-gitlab-maven) [![size](https://img.shields.io/docker/image-size/andrcuns/dependabot-gitlab-maven?label=dependabot-gitlab-maven)](https://hub.docker.com/r/andrcuns/dependabot-gitlab-maven)
* [![pulls](https://img.shields.io/docker/pulls/andrcuns/dependabot-gitlab-gradle)](https://hub.docker.com/r/andrcuns/dependabot-gitlab-gradle) [![size](https://img.shields.io/docker/image-size/andrcuns/dependabot-gitlab-gradle?label=dependabot-gitlab-gradle)](https://hub.docker.com/r/andrcuns/dependabot-gitlab-gradle)
* [![pulls](https://img.shields.io/docker/pulls/andrcuns/dependabot-gitlab-cargo)](https://hub.docker.com/r/andrcuns/dependabot-gitlab-cargo) [![size](https://img.shields.io/docker/image-size/andrcuns/dependabot-gitlab-cargo?label=dependabot-gitlab-cargo)](https://hub.docker.com/r/andrcuns/dependabot-gitlab-cargo)
* [![pulls](https://img.shields.io/docker/pulls/andrcuns/dependabot-gitlab-composer)](https://hub.docker.com/r/andrcuns/dependabot-gitlab-composer) [![size](https://img.shields.io/docker/image-size/andrcuns/dependabot-gitlab-composer?label=dependabot-gitlab-composer)](https://hub.docker.com/r/andrcuns/dependabot-gitlab-composer)
* [![pulls](https://img.shields.io/docker/pulls/andrcuns/dependabot-gitlab-mix)](https://hub.docker.com/r/andrcuns/dependabot-gitlab-mix) [![size](https://img.shields.io/docker/image-size/andrcuns/dependabot-gitlab-mix?label=dependabot-gitlab-mix)](https://hub.docker.com/r/andrcuns/dependabot-gitlab-mix)
* [![pulls](https://img.shields.io/docker/pulls/andrcuns/dependabot-gitlab-nuget)](https://hub.docker.com/r/andrcuns/dependabot-gitlab-nuget) [![size](https://img.shields.io/docker/image-size/andrcuns/dependabot-gitlab-nuget?label=dependabot-gitlab-nuget)](https://hub.docker.com/r/andrcuns/dependabot-gitlab-nuget)
* [![pulls](https://img.shields.io/docker/pulls/andrcuns/dependabot-gitlab-bundler)](https://hub.docker.com/r/andrcuns/dependabot-gitlab-bundler) [![size](https://img.shields.io/docker/image-size/andrcuns/dependabot-gitlab-bundler?label=dependabot-gitlab-bundler)](https://hub.docker.com/r/andrcuns/dependabot-gitlab-bundler)
* [![pulls](https://img.shields.io/docker/pulls/andrcuns/dependabot-gitlab-gitsubmodule)](https://hub.docker.com/r/andrcuns/dependabot-gitlab-gitsubmodule) [![size](https://img.shields.io/docker/image-size/andrcuns/dependabot-gitlab-gitsubmodule?label=dependabot-gitlab-gitsubmodule)](https://hub.docker.com/r/andrcuns/dependabot-gitlab-gitsubmodule)
* [![pulls](https://img.shields.io/docker/pulls/andrcuns/dependabot-gitlab-elm)](https://hub.docker.com/r/andrcuns/dependabot-gitlab-elm) [![size](https://img.shields.io/docker/image-size/andrcuns/dependabot-gitlab-elm?label=dependabot-gitlab-elm)](https://hub.docker.com/r/andrcuns/dependabot-gitlab-elm)
* [![pulls](https://img.shields.io/docker/pulls/andrcuns/dependabot-gitlab-docker)](https://hub.docker.com/r/andrcuns/dependabot-gitlab-docker) [![size](https://img.shields.io/docker/image-size/andrcuns/dependabot-gitlab-docker?label=dependabot-gitlab-docker)](https://hub.docker.com/r/andrcuns/dependabot-gitlab-docker)
* [![pulls](https://img.shields.io/docker/pulls/andrcuns/dependabot-gitlab-terraform)](https://hub.docker.com/r/andrcuns/dependabot-gitlab-terraform) [![size](https://img.shields.io/docker/image-size/andrcuns/dependabot-gitlab-terraform?label=dependabot-gitlab-terraform)](https://hub.docker.com/r/andrcuns/dependabot-gitlab-terraform)
* [![pulls](https://img.shields.io/docker/pulls/andrcuns/dependabot-gitlab-pub)](https://hub.docker.com/r/andrcuns/dependabot-gitlab-pub) [![size](https://img.shields.io/docker/image-size/andrcuns/dependabot-gitlab-pub?label=dependabot-gitlab-pub)](https://hub.docker.com/r/andrcuns/dependabot-gitlab-pub)
* [![pulls](https://img.shields.io/docker/pulls/andrcuns/dependabot-gitlab-gomod)](https://hub.docker.com/r/andrcuns/dependabot-gitlab-gomod) [![size](https://img.shields.io/docker/image-size/andrcuns/dependabot-gitlab-gomod?label=dependabot-gitlab-gomod)](https://hub.docker.com/r/andrcuns/dependabot-gitlab-gomod)

## Changelog

Detailed changes for releases can be found in [CHANGELOG](./CHANGELOG.md) file or on [Releases](https://gitlab.com/dependabot-gitlab/dependabot/-/releases) page

## Contribution

* Take a look at [contribution](./CONTRIBUTING.md) guidelines

## Support

If you find this project useful, you can help me cover hosting costs of my `dependabot-gitlab` test instance:

[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/B0B1CI3AV)
