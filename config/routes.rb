# frozen_string_literal: true

require "sidekiq/web"
require "sidekiq/cron/web"

Sidekiq::Web.use(Auth)

Rails.application.routes.draw do
  Healthcheck.routes(self)

  concern :paginatable do
    get "(page/:page)", action: :index, on: :collection, as: ""
  end

  mount Sidekiq::Web, at: "/sidekiq"
  mount AuthHelper.with_auth(Yabeda::Prometheus::Exporter), at: "/metrics", via: :get if AppConfig.metrics?
  mount API => "/"

  root AppConfig.anonymous_access ? "projects#index" : "sessions#new"

  get "sign_in", to: "sessions#new"
  post "sign_in", to: "sessions#create"
  delete "logout", to: "sessions#destroy"

  put "/jobs/:id/execute", to: "job#execute", as: "job_execute"

  get "new_project", to: "projects#new"
  resources :projects, only: %i[index create update destroy], concerns: :paginatable
end
